import java.util.Random;
public class Numenor {

    public static boolean willSendHelp() {
      
      Random helpStatus = new Random();
      boolean helpYesOrNo= helpStatus.nextBoolean();
      return helpYesOrNo;
    }

    public static int gatherSoldiers(int booleanstatus) {
        Random random = new Random();
        
        int lowestNumberOfSoldiers =0;
        int highestNumberOfSoldiers = 0;;
        
        if (booleanstatus >= 3){
            lowestNumberOfSoldiers = 1000;
           highestNumberOfSoldiers = 3000;
            
        } else  if (booleanstatus <=2) {
            lowestNumberOfSoldiers = 200;
            highestNumberOfSoldiers = 600;
            
        }
        int numberOfSoldiers = random.nextInt(( highestNumberOfSoldiers-lowestNumberOfSoldiers )) + lowestNumberOfSoldiers;
        
        return numberOfSoldiers;
    }
    
    public static int calculateFleet(int theNumberOfSoldiers) {
      
        int theRemainderOfSoldiers=(theNumberOfSoldiers+2)%200;
        int theNumberOfShips = (theNumberOfSoldiers+2)/200;
        
        if ( theRemainderOfSoldiers < 50){
            return theNumberOfShips;
        }
        return theNumberOfShips+1;
        }
    
    public static int finalArmySize(int numberOfSoldiers) {
        int numberOfDismissedSoldiers = (numberOfSoldiers)%200;
        
        if(numberOfDismissedSoldiers >= 50){
           numberOfDismissedSoldiers=0;
        }
        int finalNumberOfSoldiers = numberOfSoldiers-numberOfDismissedSoldiers;
        System.out.println("Without including me and Galadriel, the number of dismissed soldiers is "+numberOfDismissedSoldiers);
        return finalNumberOfSoldiers;
           
    }

    public static void main(String[] args) {
      
      int numberOfHelps = 0;
      int i=0;
      
        while(i<5) {
          
          boolean helpstatus = willSendHelp();
          
          if (helpstatus){
           numberOfHelps++;
          }else{
            numberOfHelps += 0;
          }
          
          i++; 
        }
  
        int initialAmountOfSoldiers = gatherSoldiers(numberOfHelps);
        System.out.println("the initial number of soldiers : "+initialAmountOfSoldiers);

        int numberOfShipsNeeded = calculateFleet(initialAmountOfSoldiers);
        System.out.println("the number of ships that are needed: "+numberOfShipsNeeded);

        int theFinalSizeOfArmy = finalArmySize(initialAmountOfSoldiers);
        System.out.println("number of final Army Size : "+theFinalSizeOfArmy);
    }
}
